import axios from 'axios';
import {formatVideos} from '../utils/helper';

const KEY = process.env.REACT_APP_YOUTUBE_API_KEY;

const axiosInstance = axios.create({
  baseURL: "https://www.googleapis.com/youtube/v3/"
});

if(process.env.NODE_ENV === 'development') {
  axiosInstance.interceptors.request.use(request => {
    console.log('Starting Request:', request);
    return request;
  });
  axiosInstance.interceptors.response.use(response => {
    console.log('Response:', response);
    return response;
  });
};

export const searchVideos = async (searchTerm) => {
  try {
    let res = await axiosInstance.get('/search', {
      params: {
        q: searchTerm,
        part: 'snippet',
        maxResults: 5,
        key: KEY
      },
      transformResponse: [].concat(
        axios.defaults.transformResponse,
        (data) => {
          return formatVideos(data);
        }
      )
    })
    return res.data
  } catch (err) {
    console.error(err.response);
  }
  return [];
};
