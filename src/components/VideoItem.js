import React from 'react';

const VideoItem = ({video , handleVideoSelect}) => {
  const {thumbnail, title, description, channelTitle} = video;

  return (
    <div className="media" onClick={() => handleVideoSelect(video)} data-qa="video-item">
      <figure className="media-left">
        <figure className="image" style={{width: 128, height: 72}}>
          <img src={thumbnail} alt={description} data-qa="video-item-image"/>
        </figure>
      </figure>
      <div className="media-content">
        <div className="content">
          <p data-qa="video-item-title"><strong>{title}</strong></p>
          <p data-qa="video-item-channel">{channelTitle}</p>
        </div>
      </div>
    </div>
  );
}

export default VideoItem;
