import React, {useState} from 'react';

const SearchBar = ({handleFormSubmit}) => {
  const [term, setTerm] = useState("");

  const handleChange = (event) => {
    setTerm(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    handleFormSubmit(term);
  };

  return (
    <form onSubmit={handleSubmit} data-qa="search-form">
      <div className="field has-addons">
        <div className="control" style={{width: '100%'}}>
          <input
            className="input"
            type="text"
            placeholder="Search title..."
            onChange={handleChange}
            value={term}
            data-qa="search-input"
          />
        </div>
        <div className="control">
          <button type="submit" className="button is-info" data-qa="search-button">
            Search
          </button>
        </div>
      </div>
    </form>
  );
}

export default SearchBar;
