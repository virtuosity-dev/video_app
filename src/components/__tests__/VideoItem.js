import React from 'react';
import {shallow} from 'enzyme';
import VideoItem from '../VideoItem';
import {sampleVideos} from '../../utils/helper';

const sampleVideo = sampleVideos[0];

describe("Test VideoItem component", () => {
  it("renders with a video without crashing", () => {
    shallow(<VideoItem video={sampleVideo} handleVideoSelect={jest.fn()}/>);
  });

  it("handles \"handleVideoSelect\" function correctly when clicked", () => {
    const fnClick = jest.fn();
    const wrapper = shallow(<VideoItem video={sampleVideo} handleVideoSelect={fnClick}/>);
    wrapper.find('[data-qa="video-item"]').simulate('click');
    expect(fnClick).toHaveBeenCalledWith(sampleVideo);
  });
});
