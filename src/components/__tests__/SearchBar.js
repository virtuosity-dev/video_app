import React from "react";
import {shallow} from "enzyme";
import SearchBar from "../SearchBar";

describe("Test SearchBar component", () => {
  it("renders without crashing", () => {
    shallow(<SearchBar handleFormSubmit={jest.fn()}/>);
  });

  it("handles \"handleFormSubmit\" function correctly when form is submitted", () => {
    const fnSubmit = jest.fn();
    const searchTerm = "test_term";
    const wrapper = shallow(<SearchBar handleFormSubmit={fnSubmit}/>);
    wrapper.find('[data-qa="search-input"]').simulate('change', {target: {value: searchTerm}});
    wrapper.find('[data-qa="search-form"]').simulate('submit', { preventDefault () {} });
    expect(fnSubmit).toHaveBeenCalledWith(searchTerm);
  });
});
