import React from 'react';
import {shallow} from 'enzyme';
import VideoList from '../VideoList';
import {sampleVideos} from '../../utils/helper';

describe("Test VideoList component", () => {
  it("renders without a list of video without crashing", () => {
    shallow(<VideoList videos={[]} handleVideoSelect={jest.fn()}/>);
  });

  it("renders with a list of video without crashing", () => {
    shallow(<VideoList videos={sampleVideos} handleVideoSelect={jest.fn()}/>);
  });

  it("displays a list with the correct number of \"VideoItem\"s", () => {
    const wrapper = shallow(<VideoList videos={sampleVideos} handleVideoSelect={jest.fn()}/>);
    expect(wrapper.exists('[data-qa="video-list"]')).toBeTruthy();
    expect(wrapper.find('VideoItem')).toHaveLength(sampleVideos.length);
  });
})
