import React from 'react';
import {shallow} from 'enzyme';
import VideoDetail from '../VideoDetail';
import {sampleVideos} from '../../utils/helper';

const sampleVideo = sampleVideos[0];

describe("Test VideoDetail component", () => {
  it("renders without a video without crashing", () => {
    shallow(<VideoDetail/>);
  });

  it("renders with a video without crashing", () => {
    shallow(<VideoDetail selectedVideo={sampleVideo}/>);
  });

  it("displays placeholder message when there is nothing selected", () => {
    const wrapper = shallow(<VideoDetail/>);
    expect(wrapper.text()).toEqual("Search and select a video...");
    expect(wrapper.exists('[data-qa="video-iframe"]')).toBeFalsy();
    expect(wrapper.exists('[data-qa="video-title"]')).toBeFalsy();
    expect(wrapper.exists('[data-qa="video-desc"]')).toBeFalsy();
  });

  it("displays the video, its title and description, when a video is selected", () => {
    const wrapper = shallow(<VideoDetail selectedVideo={sampleVideo}/>);
    expect(wrapper.text()).not.toEqual("Loading ...");
    expect(wrapper.exists('[data-qa="video-iframe"]')).toBeTruthy();
    expect(wrapper.exists('[data-qa="video-title"]')).toBeTruthy();
    expect(wrapper.exists('[data-qa="video-desc"]')).toBeTruthy();
  });
})
