import React from 'react';
import VideoItem from './VideoItem';

const VideoList = ({videos, handleVideoSelect}) => {
  return (
    <div className="panel" data-qa="video-list">
      {
        videos.map((video) => {
          return (
            <span className="panel-block video-item-container" key={video.videoId}>
              <VideoItem video={video} handleVideoSelect={handleVideoSelect}/>
            </span>
          );
        })
      }
    </div>
  );
}

export default VideoList;
