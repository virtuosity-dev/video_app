import React from 'react';

const VideoDetail = ({selectedVideo}) => {
  if (!selectedVideo) {
    return (<div>Search and select a video...</div>);
  }
  
  const {videoId, title, description} = selectedVideo;
  const videoSrc = `https://www.youtube.com/embed/${videoId}`;

  return (
    <div className="container">
      <div className="columns">
        <div className="column">
          <div className="video-container">
            <iframe width="640" height="360" src={videoSrc} allowFullScreen title='Video player' data-qa="video-iframe"/>
          </div>
        </div>
      </div>
      <div className="columns">
        <div className="column">
          <div className="content">
            <h4 className="title is-4" data-qa="video-title">{title}</h4>
            <p className="subtitle is-6" data-qa="video-desc">{description}</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default VideoDetail;
