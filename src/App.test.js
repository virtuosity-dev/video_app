import React from "react";
import {shallow, mount} from "enzyme";
import App from "./App";
import * as client from './apis/client';
import {sampleVideos} from './utils/helper';
import { act } from "react-dom/test-utils";

const flushPromises = () => new Promise(setImmediate);

afterEach(() => {
  jest.clearAllMocks();
});

describe("Test App", () => {
  it("renders without crashing", () => {
    shallow(<App/>);
  });

  it("searches from videos, and display selected video correctly", async () => {
    const app = mount(<App/>);
    const searchVideosSpy = jest.spyOn(client, 'searchVideos')
    searchVideosSpy.mockImplementation((searchTerm) => Promise.resolve(sampleVideos));
    const searchTerm = "test_term";

    expect(app.exists('[data-qa="search-input"]')).toBeTruthy();
    expect(app.exists('[data-qa="search-form"]')).toBeTruthy();
    app.find('[data-qa="search-input"]').simulate('change', {target: {value: searchTerm}});
    app.find('[data-qa="search-form"]').simulate('submit', { preventDefault () {} });
    expect(searchVideosSpy).toHaveBeenCalledWith(searchTerm);
    await act(async () => {
      await flushPromises();
    });
    app.update();
    expect(app.exists('[data-qa="video-list"]')).toBeTruthy();
    expect(app.find('[data-qa="video-item"]')).toHaveLength(sampleVideos.length);
    expect(app.find('[data-qa="video-item"]').first().find('[data-qa="video-item-title"]').text()).toEqual(sampleVideos[0].title);
    app.find('[data-qa="video-item"]').first().simulate('click');
    expect(app.find('[data-qa="video-details"]').text()).not.toEqual("Loading ...");
    expect(app.find('[data-qa="video-details"]').find('[data-qa="video-title"]').text()).toEqual(sampleVideos[0].title);
  });
});
