export const formatVideos = (results) => {
  const videoArray = results.items || [];
  const videoArrayFormatted = videoArray.map((video) => {
    return {
      videoId: video.id.videoId,
      thumbnail: video.snippet.thumbnails.medium.url,
      title: video.snippet.title,
      description: video.snippet.description,
      channelTitle: video.snippet.channelTitle
    }
  });
  return videoArrayFormatted;
};

export const sampleVideos = [
  {
    videoId: "zAGVQLHvwOY",
    thumbnail: "https://i.ytimg.com/vi/zAGVQLHvwOY/mqdefault.jpg",
    title: "JOKER - Final Trailer - Now Playing In Theaters",
    description: "https://www.joker.movie https://www.facebook.com/jokermovie https://twitter.com/jokermovie https://www.instagram.com/jokermovie/ Director Todd Phillips ...",
    channelTitle: "Warner Bros. Pictures"
  }, {
    videoId: "QntBkDFkiuY",
    thumbnail: "https://i.ytimg.com/vi/QntBkDFkiuY/mqdefault.jpg",
    title: "Will Smith Surprises Viral Video Classmates for Their Kindness",
    description: "Ellen welcomed high school students Kristopher and Antwain, who surprised their bullied classmate, Micheal, with new clothes and shoes. A video of the kind ...",
    channelTitle: "TheEllenShow"
  }, {
    videoId: "FHYtj1m_5QI",
    thumbnail: "https://i.ytimg.com/vi/FHYtj1m_5QI/mqdefault.jpg",
    title: "Ellen Shocks Jeannie With a Huge Surprise",
    description: "It's been 10 years since Ellen gave Jeannie her first-ever job, and to celebrate the milestone, Ellen shocked Jeannie with an unforgettable surprise.",
    channelTitle: "TheEllenShow"
  }, {
    videoId: "29a6o5vRKVM",
    thumbnail: "https://i.ytimg.com/vi/29a6o5vRKVM/mqdefault.jpg",
    title: "Marshmello &amp; Kane Brown - One Thing Right (Official Music Video)",
    description: "Marshmello & Kane Brown - One Thing Right (Official Music Video) Download / Stream 'One Thing Right' ▷ https://smarturl.it/melloxkb Shop The Official 'One ...",
    channelTitle: "Marshmello"
  }, {
    videoId: "w2Ov5jzm3j8",
    thumbnail: "https://i.ytimg.com/vi/w2Ov5jzm3j8/mqdefault.jpg",
    title: "Lil Nas X - Old Town Road (Official Movie) ft. Billy Ray Cyrus",
    description: "Official video for Lil Nas X's Billboard #1 hit, “Old Town Road (Remix)” featuring Billy Ray Cyrus. Special guest appearances from Chris Rock, Haha Davis, Rico ...",
    channelTitle: "LilNasXVEVO"
  }
];
