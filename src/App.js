import React, {useState} from 'react';
import './App.sass';
import VideoDetail from './components/VideoDetail';
import SearchBar from './components/SearchBar';
import VideoList from './components/VideoList';
import {searchVideos} from './apis/client';

function App() {
  const [videos, setVideos] = useState([]);
  const [selectedVideo, setSelectedVideo] = useState(undefined);

  const handleFormSubmit = async (searchTerm) => {
    const results = await searchVideos(searchTerm);
    setVideos(results);
  }

  const handleVideoSelect = (video) => {
    setSelectedVideo(video);
  }

  return (
    <div className="container">
      <div className="columns">
        <div className="column">
          <SearchBar handleFormSubmit={handleFormSubmit}/>
        </div>
      </div>
      <div className="columns">
        <div className="column is-two-thirds">
          <VideoDetail selectedVideo={selectedVideo} data-qa="video-details"/>
        </div>
        <div className="column is-one-third">
          <VideoList handleVideoSelect={handleVideoSelect} videos={videos}/>
        </div>
      </div>
    </div>
  );
}

export default App;
